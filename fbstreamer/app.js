const { Cluster } = require('puppeteer-cluster');
const devices = require('puppeteer/DeviceDescriptors');
const puppeteer = require('puppeteer-firefox');
// const puppeteer = require('puppeteer');
// const pptr = require('puppeteer-core');
const fs = require('fs').promises;

(async () => {
    // const iPhone = devices['iPhone 6'];
    const cluster = await Cluster.launch({
        concurrency: Cluster.CONCURRENCY_CONTEXT,
        maxConcurrency: 100,
        timeout: 180000000,
        monitor: false,
        puppeteer,
        puppeteerOptions: {
            headless: false,
            // executablePath: 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe',
            // args: [
            //     '--auto-open-devtools-for-tabs',
            //     '--disable-dev-shm-usage'
            // ]
        
        }
    });

    // Extracts document.title of the crawled pages
    await cluster.task(async ({ page, data: payload }) => {
        // await page.emulate(iPhone)
        await page.goto(payload.fburl, { waitUntil: 'domcontentloaded', timeout: 0 })
        // await page.setViewport({width: 0, height: 0});
        await page.waitFor('input[name="email"]');
        await page.$eval('input[name="email"]', (el, payload) => el.value = payload.username, payload)
        await page.waitFor(1000)
        await page.waitFor('input[name="pass"]');
        await page.$eval('input[name="pass"]', (el, payload) => el.value = payload.password, payload)
        await page.waitFor(1000)
        await page.waitFor('#loginbutton', {timeout:0});
        await page.click('#loginbutton')
        console.log('submit:',payload.username)
        await page.waitFor(10000)
        await page.goto(payload.videourl)
        // // await page.waitForSelector('#fb_stories_card_root', {timeout: 18000000})
        // await page.type('input[name="email"]', payload.username, { delay: 50 })
        // await page.waitFor(2000)
        // await page.type('input[name="pass"]', payload.password, { delay: 50 })
        // await page.waitFor(2000)
        // await page.waitFor('input[name="login"]', {timeout:100000});
        // await page.click('button[name="login"]');
        // await page.waitForNavigation({ waitUntil: 'domcontentloaded' })
        // await page.waitFor(6000)
        // await page.waitForSelector('#pagelet_dock +div div', {timeout: 18000000})
        // const inner = await page.$eval('#pagelet_dock +div div', e => e.innerHTML);
        
        await page.waitForSelector('#root .widePic', {timeout: 18000000})
        const inner = await page.$eval('#root .widePic', e => e.innerHTML);
        console.log(inner)

        // await page.click('#root .widePic video')
        // await page.keyboard.press('Tab')
        // await page.keyboard.press('Tab')
        // await page.keyboard.press('Space')
        const pageTitle = await page.evaluate(() => document.title)
        console.log(`Page title of ${payload.fburl} is ${pageTitle}`)
        // const hrefElement = await page.$('video')
        // await hrefElement.click();
        // await page.click('video')
        // for (let i = 0; i < 20; i++) {
        //     console.log('refresh:',payload.username)
        //     await page.waitFor(90000)
        // }
        
        // //delete element
        // await page.screenshot({
        //     path: 'pic.png'
        // }); //for testing purposes

        // await page.goto(payload.videourl, {
        //     waitUntil: 'domcontentloaded',
        //     timeout: 1800000
        // })
        await page.waitForSelector('._6445', {
            timeout: 18000000
        })
        //delete commment section
        await page.evaluate(() => {
            let dam = document.querySelector('._6445');
            dam.innerHTML = "CLOSED BY BOT"
         });
         //delete facebook bar
        await page.evaluate(() => {
            let dim = document.querySelector('#blueBarDOMInspector');
            dim.innerHTML = "CLOSED BY BOT"
         });
                //   delete facebook page sidebar
        await page.evaluate(() => {
            let dom = document.querySelector('#ChatTabsPagelet');
            dom.innerHTML = "CLOSED BY BOT"
         });
        //   //delete facebook buddy list
        // await page.evaluate(() => {
        //     let dom = document.querySelector('#BuddylistPagelet');
        //     dom.innerHTML = "CLOSED BY BOT"
        //  });
        //setting button
        //#js_h0
        //set quality
        //._2iw4 
        //240p
        //a last child
        await page.waitFor(18000000)
        // await page.waitForSelector('#pagelet_dock +div div', {
        //     timeout: 18000000
        // })
    });

    // In case of problems, log them
    cluster.on('taskerror', (err, data) => {
        console.log(`Error crawling ${data}: ${err.message}`);
    });

    // Read the top-1m.csv file from the current directory
    const fburl = 'https://www.facebook.com/'
    const videourl = 'https://www.facebook.com/Mahercogaming/videos/1005582126465579/'
    const csvFile = await fs.readFile(__dirname + '/facebook-credentials.csv', 'utf8');
    const lines = csvFile.split('\n');
    for (let i = 0; i < lines.length-1; i++) {
    // for (let j = 0; j < 2; j++) {        
        const obj = lines[i].split(';')
        var cred = {
            username: obj[0],
            password: obj[1]
        }
        console.log(i, cred)
        cluster.queue({
            fburl: fburl,
            videourl: videourl,
            username: cred.username,
            password: cred.password
        })
    // }
    }

    await cluster.idle();
    await cluster.close();
})();